# Using Issues and Epics with Sourcegraph Batch Changes

This is a set of resources that demonstrates how a Sourcegraph Batch Change user might create Issues as part of the Batch Change process, assign those Issues to an Epic, and link to those issues from the opened Merge Requests. This approach will scale to hundreds or even thousands of projects. For this demonstration, 3 open-source repositories were forked on Gitlab and used as example targets for Batch Changes.

The Batch Change UI in Sourcegraph can be used to track the status of the opened Merge Requests, as well as to perform bulk operations such as adding comments.

![Batch Change screenshot](/assets/batch-change.png)

The example here runs a script once per changeset, which results in an Issue being assigned to the Epic that was created to track this initiative.

![Epic screenshot](/assets/epic.png)

The batch change template language allows outputs from the script to be included in the Merge Request - in this case, we link the created Issue from the description of the opened Merge Request.

![Merge Request screenshot](/assets/merge-request.png)

## Prerequisites

1. You have a Sourcegraph instance with permissions to use Batch Changes for the targeted repositories.
2. You have Gitlab Premium or Ultimate and permission to create Issues and update the target Epic.
3. You've created an Epic, to which the created Issues will be assigned.
4. You've set up the Sourcegraph Batch Change feature settings with your Gitlab credentials.

## Premise

The premise for this example is an initiative to standardize on the pnpm package manager, as an alternative to npm and yarn for JavaScript and TypeScript projects. This involves a few steps for every location of a package.json file:

1. If workspaces are in use, then create the pnpm-workspaces.yaml file. 
2. Add `npx only-allow pnpm` to the preinstall scripts in package.json.
3. Run `pnpm import` in every directory where a package.json file exists
4. Run a script to create an Issue for every Merge Request that is opened as a result of this batch change.

## Explanation

The [batch-spec.yaml](./batch-spec.yaml) file defines this batch change. It defines a name and description for the change, as well as a repository query to choose the target repositories to which this change will be applied. In this case, the query is selecting a number of repositories where the NodeJS version is 16 or 18, and the repositories exist in a project of my Gitlab organization.

The batch spec then defines a number of steps, each of which runs in a specified container. The Dockerfiles to create the containers are also included for convenience.

The first step executes Ruby script to accomplish steps 1-2 above.

The second step, which runs in a container where pnpm has been installed, runs the `pnpm import` command everywhere that a package.json file exists.

The third step runs in a container that has been built with the included createIssue.js script. The script takes a single argument - the repository path - and calls the Gitlab API to create an Issue which gets assigned to the target Epic, and which also gets linked in the Merge Request description.

## Steps for Use

To use this in your own Gitlab environment on your own projects, you'll want to do the following at a minimum:

1. Update environment variable values in your batch-spec.yaml file to match your Gitlab url and Epic IID.
2. Upload your Gitlab API token to an executor secret to Sourcegraph called `GITLAB_TOKEN`

You'll also probably want to modify the containers used (i.e. modifying/updating the createIssue.js script to do additional things) - be sure to build containers for the amd64 target architecture, which is what Sourcegraph Batch Changes currently run. Batch changes can be used with local containers or with containers in both public and private registries. Refer to the Sourcegraph documentation for current details on how to accomplish this.