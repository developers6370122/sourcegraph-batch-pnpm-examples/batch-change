// This script expects 1 argument: the project/repository path
// i.e. org/project/subproject/repository

// Your personal access token for the GitLab API
const token = process.env.GITLAB_TOKEN;

// The URL path for the project you want to find
const projectUrl = process.argv[2];

// The base URL for the GitLab API
const baseUrl = process.env.GITLAB_BASE_URL;

// The Epic to associate the issue with
const epic = process.env.EPIC_IID;

// Headers for all API requests
const headers = {
    'PRIVATE-TOKEN': token,
    'Content-Type': 'application/json',
};

main(baseUrl, projectUrl, headers, epic)

// Main function to fetch all projects, create an issue, and assign it to an epic
async function main(baseUrl, projectUrl, headers, epic) {
    try {
        // Fetch all projects
        const searchTerm = projectUrl.replace('gitlab.com/', '');
        const searchUrl = `${baseUrl}/search?scope=projects&search=${encodeURIComponent(searchTerm)}`;
        const projects = await fetchData(searchUrl, headers);

        // Check if projects were found
        if (projects.length == 0) {
            throw new Error(`Could not find a project with the search term '${searchTerm}'.`);
        } else if (projects.length >= 2) {
            throw new Error(`Found more than one project with the search term '${searchTerm}'.`);
        }

        // Get the target project
        const targetProject = projects[0];

        // Create an issue in the target project
        const issue = await createIssue(baseUrl, targetProject.id, headers);

        // Assign the issue to an epic
        await assignIssueToEpic(baseUrl, targetProject.namespace.id, issue.id, headers, epic);

        // Success: Issue created and assigned to an epic
        console.log(issue.web_url);
    } catch (error) {
        console.log(error)
        throw new Error('Error:', error);
    }
}

// Function to fetch JSON data from a URL
async function fetchData(url, headers) {
    const response = await fetch(url, { headers });
    if (!response.ok) {
        throw new Error(`Request to ${url} failed with status: ${response.status}`);
    }
    return response.json();
}

// Function to create an issue in the target project
async function createIssue(baseUrl, projectId, headers) {
    const createIssueUrl = `${baseUrl}/projects/${projectId}/issues`;
    const response = await fetch(createIssueUrl, {
        method: 'POST',
        headers,
        body: JSON.stringify({
            title: `Update ${projectUrl} to use pnpm`,
            description: "This issue is part of the initiative to standardize on pnpm as our preferred package manager",
        }),
    });
    if (!response.ok) {
        throw new Error(`Request to ${createIssueUrl} failed with status: ${response.status}`);
    }
    return response.json();
}

// Function to assign the issue to an epic
async function assignIssueToEpic(baseUrl, namespaceId, issueId, headers, epic) {
    const issueToEpicUrl = `${baseUrl}/groups/${namespaceId}/epics/${epic}/issues/${issueId}`;
    const response = await fetch(issueToEpicUrl, {
        method: 'POST',
        headers,
    });
    if (!response.ok) {
        throw new Error(`Request to ${issueToEpicUrl} failed with status: ${response.status}`);
    }
}

