# Use the latest NodeJS image to build the container
FROM node:latest

# Copy the script to the filesystem of the container
COPY createIssue.js /createIssue.js