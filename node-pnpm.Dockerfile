# Use the latest NodeJS image to build the container
FROM node:latest

# Install pnpm
RUN npm install -g pnpm